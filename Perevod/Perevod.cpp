#include <iostream>
#include <cmath>
#include <vector>
using namespace std;
#define PI 3.14159265;
double c = 299792.458;//��/�

////////////////////////////////////////////������� ���������//////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct IGASK
{
	double x;
	double y;
	double z;
};

struct GEOD
{
	double shirota;
	double dolgota;
	double visota;
};

struct Vremya
{
	int chas;
	int min;
	double sek;
};

struct Data
{
	int year;
	int month;
	int day;
};

struct GRINVICH
{
	double x;
	double y;
	double z;
};

double Vichislenie_Zv_vremya_Grinvich_v_Polnoch(Data data)
{
	int a = (14 - data.month) / 12;
	int y = data.year + 4800 - a;
	int m = data.month + 12 * a - 3;
	int JDN = data.day + (153 * m + 2) / 5 + 365 * y + y / 4 - y / 100 + y / 400 - 32045;
	//cout << "date" << JDN << endl;
	double d = (JDN - 2451545) / 36525;
	double Scp = 1.7533685592 + 0.0172027918051*(JDN - 2451545) + 6.7707139*(1e-6)*d*d - 4.50876*(1e-10)*d*d*d;
	//double Scp = (6 * 3600 + 41 * 60 + 50.54641 + 236.555367908*(JDN - 2451545) + 0.093104*d*d - 6.2*(1e-6)*d*d*d)/180*PI;
	//cout << "Zv vremya " << Scp << endl;
	return Scp;
}

double *Pol_t_P(double xp, double yp) //������� ���������� �������� ������
{
	double mas[9] = { 1,0,xp,0,1,-yp,-xp,yp,1 };
	double *p = mas;
	return p;
}

double *Zv_vremya_P(double vremya)//������� ��������� ������� �� �������� � ������������ ����
{
	double ugol = vremya;
	double mas[9] = { cos(ugol),sin(ugol),0 ,-sin(ugol),cos(ugol),0, 0,0,1 };
	double *p = mas;
	return p;
}

/*double *Zv_vremya_P(Vremya vremya)//������� ��������� ������� �� �������� � ������������ ����
{
	double ugol = (vremya.chas * 15 + (vremya.min * 15) / 60 + (vremya.sek * 15) / 3600) /180*PI;
	double mas[9] = {cos(ugol),sin(ugol),0 ,-sin(ugol),cos(ugol),0, 0,0,1 };
	double *p = mas;
	return p;
}*/

double *Pol_t_O(double xp, double yp) //����������������� �������
{
	double mas[9] = { 1,0,-xp,0,1,yp,xp,-yp,1 };
	double *p = mas;
	return p;
}

double *Zv_vremya_O(double vremya)//����������������� �������
{
	double ugol = vremya;
	double mas[9] = { cos(ugol),-sin(ugol),0 ,sin(ugol),cos(ugol),0, 0,0,1 };
	double *p = mas;
	return p;
}

GRINVICH Perevod_is_IGASK_v_Grinvich(IGASK &koordinaty, Data data)//������������ ���� ������
{
	double *mas = Pol_t_P(0.03, 0.35);
	double pros = Vichislenie_Zv_vremya_Grinvich_v_Polnoch(data);
	//double pros = (0 * 15 + (43 * 15) / 60 + (36.5884 * 15) / 3600) / 180 * PI;
	//cout << "DATA " << data.year << " " << data.month << " " << data.day << endl;
	double *mas1 = Zv_vremya_P(pros);
	double mas11[9];

	mas11[0] = mas[0] * mas1[0] + mas[1] * mas1[3] + mas[2] * mas1[6];
	mas11[1] = mas[0] * mas1[1] + mas[1] * mas1[4] + mas[2] * mas1[7];
	mas11[2] = mas[0] * mas1[2] + mas[1] * mas1[5] + mas[2] * mas1[8];

	mas11[3] = mas[3] * mas1[0] + mas[4] * mas1[3] + mas[5] * mas1[6];
	mas11[4] = mas[3] * mas1[1] + mas[4] * mas1[4] + mas[5] * mas1[7];
	mas11[5] = mas[3] * mas1[2] + mas[4] * mas1[5] + mas[5] * mas1[8];

	mas11[6] = mas[6] * mas1[0] + mas[7] * mas1[3] + mas[8] * mas1[6];
	mas11[7] = mas[6] * mas1[1] + mas[7] * mas1[4] + mas[8] * mas1[7];
	mas11[8] = mas[6] * mas1[2] + mas[7] * mas1[5] + mas[8] * mas1[8];


	double mas2[3] = { koordinaty.x,koordinaty.y,koordinaty.z };
	GRINVICH grin;

	grin.x = mas11[0] * mas2[0] + mas11[1] * mas2[1] + mas11[2] * mas2[2];
	grin.y = mas11[3] * mas2[0] + mas11[4] * mas2[1] + mas11[5] * mas2[2];
	grin.z = mas11[6] * mas2[0] + mas11[7] * mas2[1] + mas11[8] * mas2[2];

	return grin;
}

IGASK Perevod_is_Grinvich_v_IGASK(GRINVICH &koordinaty, Data data)//������������ ���� ������
{
	double *mas = Pol_t_O(0.03, 0.35);
	double pros = Vichislenie_Zv_vremya_Grinvich_v_Polnoch(data);
	//double pros = (0 * 15 + (43 * 15) / 60 + (36.5884 * 15) / 3600) / 180 * PI;
	//cout << "DATA " << data.year << " " << data.month << " " << data.day << endl;
	//cout << pros << endl;
	double *mas1 = Zv_vremya_O(pros);
	double mas11[9];

	mas11[0] = mas[0] * mas1[0] + mas[1] * mas1[3] + mas[2] * mas1[6];
	mas11[1] = mas[0] * mas1[1] + mas[1] * mas1[4] + mas[2] * mas1[7];
	mas11[2] = mas[0] * mas1[2] + mas[1] * mas1[5] + mas[2] * mas1[8];

	mas11[3] = mas[3] * mas1[0] + mas[4] * mas1[3] + mas[5] * mas1[6];
	mas11[4] = mas[3] * mas1[1] + mas[4] * mas1[4] + mas[5] * mas1[7];
	mas11[5] = mas[3] * mas1[2] + mas[4] * mas1[5] + mas[5] * mas1[8];

	mas11[6] = mas[6] * mas1[0] + mas[7] * mas1[3] + mas[8] * mas1[6];
	mas11[7] = mas[6] * mas1[1] + mas[7] * mas1[4] + mas[8] * mas1[7];
	mas11[8] = mas[6] * mas1[2] + mas[7] * mas1[5] + mas[8] * mas1[8];


	double mas2[3] = { koordinaty.x,koordinaty.y,koordinaty.z };
	IGASK igask;

	igask.x = mas11[0] * mas2[0] + mas11[1] * mas2[1] + mas11[2] * mas2[2];
	igask.y = mas11[3] * mas2[0] + mas11[4] * mas2[1] + mas11[5] * mas2[2];
	igask.z = mas11[6] * mas2[0] + mas11[7] * mas2[1] + mas11[8] * mas2[2];

	return igask;
}

double radius_of_Earth(double phi)//������� ������ �����
{
	double Re = 6378.2;//�������������� ������ �����
	/*double Rp = 6356.86;//�������� ������ �����
	double R;//������ ����� �� ������������ ������
	R = cos(phi)*(Re - Rp) + Rp;//������ ������� �����
	cout << "Radius Earth " << R << endl;*/
	double R = Re;
	return R;
}

GEOD perevod_s_IGASK(IGASK &koordinaty, Data data)//������� � ��������������� � ������������� ����������
{
	GRINVICH grinvich = Perevod_is_IGASK_v_Grinvich(koordinaty, data);
	GEOD prom;//������������� ����������
	prom.shirota = atan(grinvich.z / sqrt(grinvich.x*grinvich.x + grinvich.y*grinvich.y))* 180.0 / PI;//������
	prom.dolgota = atan(grinvich.y / grinvich.x)* 180.0 / PI;//�������
	double shirota = prom.shirota / 180 * PI;
	prom.visota = sqrt(grinvich.x*grinvich.x + grinvich.y*grinvich.y + grinvich.z*grinvich.z) - radius_of_Earth(shirota);//������
	return prom;
}

/*GEOD perevod_s_IGASK(IGASK &koordinaty)//������� � ��������������� � ������������� ����������(�������)
{
	GEOD prom;//������������� ����������
	prom.shirota = atan(koordinaty.z / sqrt(koordinaty.x*koordinaty.x + koordinaty.y*koordinaty.y))* 180.0 / PI;//������
	prom.dolgota = atan(koordinaty.y / koordinaty.x)* 180.0 / PI;//�������
	double shirota = prom.shirota / 180 * PI;
	prom.visota = sqrt(koordinaty.x*koordinaty.x + koordinaty.y*koordinaty.y + koordinaty.z*koordinaty.z) - radius_of_Earth(shirota);//������
	return prom;
}*/

IGASK perevod_s_GEOD(GEOD &koordinaty, Data data)//������� � ������������� � ��������������� ����������
{
	koordinaty.shirota = koordinaty.shirota / 180 * PI;
	koordinaty.dolgota = koordinaty.dolgota / 180 * PI;
	double phi = koordinaty.shirota;
	double H = koordinaty.visota;
	double R = radius_of_Earth(phi);
	GRINVICH prom;//������������� ����������
	prom.x = (R + H)*cos(koordinaty.shirota)*cos(koordinaty.dolgota);//���������� X
	prom.y = (R + H)*cos(koordinaty.shirota)*sin(koordinaty.dolgota);//���������� Y
	prom.z = (R + H)*sin(koordinaty.shirota);//���������� Z
	IGASK prom1 = Perevod_is_Grinvich_v_IGASK(prom, data);
	return prom1;
}

/*IGASK perevod_s_GEOD(GEOD &koordinaty)//������� � ������������� � ��������������� ����������(�������)
{
	koordinaty.shirota = koordinaty.shirota / 180 * PI;
	koordinaty.dolgota = koordinaty.dolgota / 180 * PI;
	double phi = koordinaty.shirota;
	double H = koordinaty.visota;
	double R = radius_of_Earth(phi);
	IGASK prom;//������������� ����������
	prom.x = (R + H)*cos(koordinaty.shirota)*cos(koordinaty.dolgota);//���������� X
	prom.y = (R + H)*cos(koordinaty.shirota)*sin(koordinaty.dolgota);//���������� Y
	prom.z = (R + H)*sin(koordinaty.shirota);//���������� Z
	return prom;
}*/
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////���������-������������ �����//////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct Sputnik
{
	IGASK koord_sputnik_IGASK;//���������� �������� � �����
	GEOD koord_sputnik_GEOD;//���������� �������� � ������������� �����������
	/*vector<double> x;//���������� �� ������� ��������� ���������
	vector<double> y;
	vector<double> z;*/
};

struct Istochnik
{
	GEOD koord_istochnik_GEOD;//���������� ��������� � ������������� �����������
	IGASK koord_istochnik_IGASK;//���������� ��������� � �����
};

vector<double> reshenie_sistemy(double Cx, double Cy, double Bx, double By, double Mx, double My, double Px, double Py)
{
	double x = ((Py - Cy) / (Px - Cx)*Cx - (My - By) / (Mx - Bx)*Bx + By - Cy) / ((Py - Cy) / (Px - Cx) - (My - By) / (Mx - Bx));
	double y = (Py - Cy)*(x - Cx) / (Px - Cx)+Cy;
	vector<double> k = { x,y };
	return k;
}

vector<double> nach_priblizhenie(Sputnik sp1, Sputnik sp2, Sputnik sp3)//����� ������� ������������
{
	double seredina12_shirota = (sp1.koord_sputnik_GEOD.shirota + sp2.koord_sputnik_GEOD.shirota) / 2;
	double seredina12_dolgota = (sp1.koord_sputnik_GEOD.dolgota + sp2.koord_sputnik_GEOD.dolgota) / 2;
	double seredina13_shirota = (sp1.koord_sputnik_GEOD.shirota + sp3.koord_sputnik_GEOD.shirota) / 2;
	double seredina13_dolgota = (sp1.koord_sputnik_GEOD.dolgota + sp3.koord_sputnik_GEOD.dolgota) / 2;
	double Cx = sp3.koord_sputnik_GEOD.dolgota, Cy = sp3.koord_sputnik_GEOD.shirota
		, Bx = sp2.koord_sputnik_GEOD.dolgota, By = sp2.koord_sputnik_GEOD.shirota
		, Mx = seredina13_dolgota, My = seredina13_shirota
		, Px = seredina12_dolgota, Py = seredina12_shirota;
	vector<double> k = reshenie_sistemy(Cx, Cy, Bx, By, Mx, My, Px, Py);
	return k;
}

double function(double* x, Sputnik sp1, Sputnik sp2, Sputnik sp3, double tau12, double tau13)
{
	// ��������� �������������� �������
	// ���������� �������� �������
	// ���������� ���������� �������� ������ ������, � ��������� ������ �������� ��������� �������
	//	........
	double R = 6378.2;
	//float c = 299792.458;
	/*float x1 = 10000, y1 = 10000, z1 = 9600;
	float x2 = 10000, y2 = 10000, z2 = 10000;
	float x3 = 10000, y3 = 10000, z3 = 10400;*/
	double x1 = sp1.koord_sputnik_IGASK.x, y1 = sp1.koord_sputnik_IGASK.y, z1 = sp1.koord_sputnik_IGASK.z;
	double x2 = sp2.koord_sputnik_IGASK.x, y2 = sp2.koord_sputnik_IGASK.y, z2 = sp2.koord_sputnik_IGASK.z;
	double x3 = sp3.koord_sputnik_IGASK.x, y3 = sp3.koord_sputnik_IGASK.y, z3 = sp3.koord_sputnik_IGASK.z;

	//double tau12 = 0.00001, tau13 = 0.00003;
	//x[0] - ���������� � ���������
	//x[1] - ���������� y ���������
	//x[2] - ���������� z ���������
	double k =
		//////////////////////////////////////x^2+y^2+z^2 = R^2//////////////////////////////////////
		(x[0] * x[0] + x[1] * x[1] + x[2] * x[2] - R*R) * (x[0] * x[0] + x[1] * x[1] + x[2] * x[2] - R*R)

		//////////////////////////////////L1-L2 = c*tau12////////////////////////////////////////////
		+ (abs(sqrt((x[0] - x1)*(x[0] - x1) + (x[1] - y1)*(x[1] - y1) + (x[2] - z1)*(x[2] - z1))
			- sqrt((x[0] - x2)*(x[0] - x2) + (x[1] - y2)*(x[1] - y2) + (x[2] - z2)*(x[2] - z2))) - c*tau12)
		*(abs(sqrt((x[0] - x1)*(x[0] - x1) + (x[1] - y1)*(x[1] - y1) + (x[2] - z1)*(x[2] - z1))
			- sqrt((x[0] - x2)*(x[0] - x2) + (x[1] - y2)*(x[1] - y2) + (x[2] - z2)*(x[2] - z2))) - c*tau12)

		//////////////////////////////////L1-L3 = c*tau13////////////////////////////////////////////
		+ (abs(sqrt((x[0] - x1)*(x[0] - x1) + (x[1] - y1)*(x[1] - y1) + (x[2] - z1)*(x[2] - z1))
			- sqrt((x[0] - x3)*(x[0] - x3) + (x[1] - y3)*(x[1] - y3) + (x[2] - z3)*(x[2] - z3))) - c*tau13)
		*(abs(sqrt((x[0] - x1)*(x[0] - x1) + (x[1] - y1)*(x[1] - y1) + (x[2] - z1)*(x[2] - z1))
			- sqrt((x[0] - x3)*(x[0] - x3) + (x[1] - y3)*(x[1] - y3) + (x[2] - z3)*(x[2] - z3))) - c*tau13);
	return k;
}

double MHJ(int kk, double* x, Sputnik sp1, Sputnik sp2, Sputnik sp3, Istochnik &istochnik, double tau12, double tau13,Data data)
{
	// kk - ���������� ����������; x - ������ ����������
	double  TAU = 1.e-16f; // �������� ����������
	int i, j, bs, ps;
	double z, h, k, fi, fb;
	double *b = new double[kk];
	double *y = new double[kk];
	double *p = new double[kk];
	vector<double> koefg = nach_priblizhenie(sp1, sp2, sp3);
	GEOD prom;
	prom.dolgota = koefg[0];
	prom.shirota = koefg[1];
	prom.visota = 0;
	IGASK prom1 = perevod_s_GEOD(prom, data);
	h = 1.;
	x[0] = prom1.x;
	x[1] = prom1.y;
	x[2] = prom1.z;
	cout << prom.dolgota << " " << prom.shirota << " " << prom.visota << endl;
	cout << x[0] << " " << x[1] << " " << x[2] << endl;
	//for (i = 0; i<kk; i++)  x[i] = (float)rand() / RAND_MAX; // �������� ��������� �����������

	k = h;
	for (i = 0; i < kk; i++)	y[i] = p[i] = b[i] = x[i];
	fi = function(x, sp1, sp2, sp3, tau12, tau13);
	ps = 0;   bs = 1;  fb = fi;

	j = 0;
	while (1)
	{
		//calc++; // ������� ��������. ����� ������������

		x[j] = y[j] + k;
		z = function(x, sp1, sp2, sp3, tau12, tau13);
		if (z >= fi) {
			x[j] = y[j] - k;
			z = function(x, sp1, sp2, sp3, tau12, tau13);
			if (z < fi)   y[j] = x[j];
			else  x[j] = y[j];
		}
		else  y[j] = x[j];
		fi = function(x, sp1, sp2, sp3, tau12, tau13);

		if (j < kk - 1) { j++;  continue; }
		if (fi + 1e-16 >= fb) {
			if (ps == 1 && bs == 0) {
				for (i = 0; i < kk; i++) {
					p[i] = y[i] = x[i] = b[i];
				}
				z = function(x, sp1, sp2, sp3, tau12, tau13);
				bs = 1;   ps = 0;   fi = z;   fb = z;   j = 0;
				continue;
			}
			k /= 10;
			if (k < TAU) break;
			//double zzz = function(x, sp1, sp2, sp3, tau12, tau13);
			//if (z < 1) break;
			j = 0;
			continue;
		}

		for (i = 0; i < kk; i++) {
			p[i] = 2 * y[i] - b[i];
			b[i] = y[i];
			x[i] = p[i];

			y[i] = x[i];
		}
		z = function(x, sp1, sp2, sp3, tau12, tau13);
		fb = fi;   ps = 1;   bs = 0;   fi = z;   j = 0;
	} //  end of while(1)

	for (i = 0; i < kk; i++)  x[i] = p[i];
	cout << x[0] << " " << x[1] << " " << x[2] << " " << fb << endl;
	istochnik.koord_istochnik_IGASK.x = x[0];
	istochnik.koord_istochnik_IGASK.y = x[1];
	istochnik.koord_istochnik_IGASK.z = x[2];
	delete b;
	delete y;
	delete p;
	return fb;
}

double vremennye_zaderzhki(Istochnik istochnik, Sputnik sputnik1, Sputnik sputnik2)
{
	double L1 = sqrt((sputnik1.koord_sputnik_IGASK.x - istochnik.koord_istochnik_IGASK.x)
		*(sputnik1.koord_sputnik_IGASK.x - istochnik.koord_istochnik_IGASK.x)
		+ (sputnik1.koord_sputnik_IGASK.y - istochnik.koord_istochnik_IGASK.y)
		*(sputnik1.koord_sputnik_IGASK.y - istochnik.koord_istochnik_IGASK.y)
		+ (sputnik1.koord_sputnik_IGASK.z - istochnik.koord_istochnik_IGASK.z)
		*(sputnik1.koord_sputnik_IGASK.z - istochnik.koord_istochnik_IGASK.z));
	double L2 = sqrt((sputnik2.koord_sputnik_IGASK.x - istochnik.koord_istochnik_IGASK.x)
		*(sputnik2.koord_sputnik_IGASK.x - istochnik.koord_istochnik_IGASK.x)
		+ (sputnik2.koord_sputnik_IGASK.y - istochnik.koord_istochnik_IGASK.y)
		*(sputnik2.koord_sputnik_IGASK.y - istochnik.koord_istochnik_IGASK.y)
		+ (sputnik2.koord_sputnik_IGASK.z - istochnik.koord_istochnik_IGASK.z)
		*(sputnik2.koord_sputnik_IGASK.z - istochnik.koord_istochnik_IGASK.z));
	double tau = abs(L1 / c - L2 / c);
	return tau;
}





//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int main()
{
	setlocale(LC_ALL, "rus");
	Sputnik sputnik1;
	Sputnik sputnik2;
	Sputnik sputnik3;
	Istochnik istochnik1;
	Istochnik istochnik2;
	Data data;
	//Data data2;
	//Istochnik istochnik1;
	double tau12, tau13;//��������� ��������
	//��������� ��������� ��������� ���������:
	cout << "���� ������: " << endl;
	cout << "������� ����: ���, �����, ����" << endl;
	//cin >> data.year >> data.month >> data.day;
	data.day = 10;
	data.month = 10;
	data.year = 2017;
	cout << "���������� ���������: " << endl << "������ = ";
	cin >> istochnik1.koord_istochnik_GEOD.shirota;
	cout << "������� = ";
	cin >> istochnik1.koord_istochnik_GEOD.dolgota;
	cout << "������ = 0 � ������" << endl;
	istochnik1.koord_istochnik_GEOD.visota = 0;
	system("cls");
	cout << "���������� ���������: " << endl
		<< "������ = " << istochnik1.koord_istochnik_GEOD.shirota << endl
		<< "������� = " << istochnik1.koord_istochnik_GEOD.dolgota << endl
		<< "������ = 0 � ������" << endl
		<< "����: 2017 10 10" << endl;//!!!!!!!!!!!!!!!!!�������� ������ � ���� �����
	sputnik1.koord_sputnik_IGASK.x = 10207.1, sputnik1.koord_sputnik_IGASK.y = 8706.89, sputnik1.koord_sputnik_IGASK.z = 9394.15;//������ 1 ��������
	sputnik2.koord_sputnik_IGASK.x = 10764, sputnik2.koord_sputnik_IGASK.y = 10979, sputnik2.koord_sputnik_IGASK.z = 8099.3;//������ 2 ��������
	sputnik3.koord_sputnik_IGASK.x = 10847.9, sputnik3.koord_sputnik_IGASK.y = 9253.45, sputnik3.koord_sputnik_IGASK.z = 5760.78;//������ 3 ��������
	sputnik1.koord_sputnik_GEOD = perevod_s_IGASK(sputnik1.koord_sputnik_IGASK, data);
	sputnik2.koord_sputnik_GEOD = perevod_s_IGASK(sputnik2.koord_sputnik_IGASK, data);
	sputnik3.koord_sputnik_GEOD = perevod_s_IGASK(sputnik3.koord_sputnik_IGASK, data);
	cout << "���������� ������� ��������: " << endl;
	cout << "x = " << sputnik1.koord_sputnik_IGASK.x << endl
		<< "y = " << sputnik1.koord_sputnik_IGASK.y << endl
		<< "z = " << sputnik1.koord_sputnik_IGASK.z << endl
		<< "������ = " << sputnik1.koord_sputnik_GEOD.shirota << endl
		<< "������� = " << sputnik1.koord_sputnik_GEOD.dolgota << endl
		<< "������ = " << sputnik1.koord_sputnik_GEOD.visota << endl;
	cout << "���������� ������� ��������: " << endl;
	cout << "x = " << sputnik2.koord_sputnik_IGASK.x << endl
		<< "y = " << sputnik2.koord_sputnik_IGASK.y << endl
		<< "z = " << sputnik2.koord_sputnik_IGASK.z << endl
		<< "������ = " << sputnik2.koord_sputnik_GEOD.shirota << endl
		<< "������� = " << sputnik2.koord_sputnik_GEOD.dolgota << endl
		<< "������ = " << sputnik2.koord_sputnik_GEOD.visota << endl;
	cout << "���������� �������� ��������: " << endl;
	cout << "x = " << sputnik3.koord_sputnik_IGASK.x << endl
		<< "y = " << sputnik3.koord_sputnik_IGASK.y << endl
		<< "z = " << sputnik3.koord_sputnik_IGASK.z << endl
		<< "������ = " << sputnik3.koord_sputnik_GEOD.shirota << endl
		<< "������� = " << sputnik3.koord_sputnik_GEOD.dolgota << endl
		<< "������ = " << sputnik3.koord_sputnik_GEOD.visota << endl;
	cout << "������� ��������� ��������: " << endl;
	istochnik1.koord_istochnik_IGASK = perevod_s_GEOD(istochnik1.koord_istochnik_GEOD, data);
	//tau12 = 0.00333568814, tau13 = 0.0033356;
	tau12 = vremennye_zaderzhki(istochnik1, sputnik1, sputnik2);
	tau13 = vremennye_zaderzhki(istochnik1, sputnik1, sputnik3);
	cout << "Tau12 = " << tau12 << endl << "Tau13 = " << tau13 << endl;
	cout << "������ ��������� ��������� �� ���� ������" << endl;
	double *x = new double[3];
	double pogreshnost = MHJ(3, x, sputnik1, sputnik2, sputnik3, istochnik2, tau12, tau13,data);
	istochnik2.koord_istochnik_GEOD = perevod_s_IGASK(istochnik2.koord_istochnik_IGASK, data);
	cout << "����������� ���������� ���������: " << endl
		<< "������ = " << istochnik2.koord_istochnik_GEOD.shirota << endl
		<< "������� = " << istochnik2.koord_istochnik_GEOD.dolgota << endl
		<< "������ = " << istochnik2.koord_istochnik_GEOD.visota << endl;
	/*IGASK koord_sputnik;
	GEOD koord_istochnik;
	GEOD vivod1;
	IGASK vivod2;
	Data data1;
	Data data2;
	//������� �� ���� � ������������� ����������
	cout << "Enter koordinaty IGASK: x,y,z" << endl;
	cin >> koord_sputnik.x >> koord_sputnik.y >> koord_sputnik.z;
	cout << "Enter data IGASK: year,month,day" << endl;
	cin >> data1.year >> data1.month >> data1.day;
	vivod1 = perevod_s_IGASK(koord_sputnik, data1);
	cout << "Vivod v GEOD: " << endl;
	cout << "Shirota " << vivod1.shirota << endl;
	cout << "Dolgota " << vivod1.dolgota << endl;
	cout << "Visota " << vivod1.visota << endl;
	//������� �� ������������� ��������� � ����
	cout << "Enter koordinaty GEOD: shirota,dolgota,visota" << endl;
	cin >> koord_istochnik.shirota >> koord_istochnik.dolgota >> koord_istochnik.visota;
	cout << "Enter data GEOD: year,month,day" << endl;
	cin >> data2.year >> data2.month >> data2.day;
	vivod2 = perevod_s_GEOD(koord_istochnik, data2);
	cout << "Vivod v IGASK: " << endl;
	cout << "x " << vivod2.x << endl;
	cout << "y " << vivod2.y << endl;
	cout << "z " << vivod2.z << endl;*/
	system("pause");
	return 0;
}